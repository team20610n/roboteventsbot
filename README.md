# Opsdroid config
I'm using the matrix connecter. Not that it should matter in this case.
```
 ## Sqlite (core)
 sqlite:
   path: "~/Git/opsdroid/default.db"  # (optional) default "~/.opsdroid/sqlite.db"
   table: "opsdroid"  # (optional) default "opsdroid"
#
#  ## Mongo (core)
#  mongo:
#    host:       "my host"     # (optional) default "localhost"
#    port:       "12345"       # (optional) default "27017"
#    database:   "mydatabase"  # (optional) default "opsdroid"
#
#  matrix:
#    default_room: "#room:server.com"
#    single_state_key: False
#    should_encrypt: True


## Skill modules
skills:
  ## Hello (https://github.com/opsdroid/skill-hello)
  hello: {}
  
  ## Seen (https://github.com/opsdroid/skill-seen)
  # seen: {}
  REvents:
    path: ~/Git/opsdroid/
```