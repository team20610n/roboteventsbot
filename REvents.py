from opsdroid.matchers import match_regex, match_always
from datetime import datetime
from ago import human
import logging
# import RobotEventsV2Py

missingDataMessage='Missing required data. To get a list of commands !help'
defaultErrorMessage=" Please check your input and try again.\nIf the problem persists please file a issue at\n https://gitlab.com/team-20610n/roboteventsmatrixbot"

APIKey = None
with open("RobotEventsKey", 'r') as _APIKey:
    APIKey = _APIKey.read()
# RobotEvents = RobotEventsV2Py.RobotEvents(APIKey)

async def GetTeam(opsdroid, TeamNumber):
    response = await opsdroid.memory.get(TeamNumber.upper())
    if (response == None):
        # response = RobotEvents.getRaw("teams?number%5B%5D=" + TeamNumber.upper())
        await opsdroid.memory.put(TeamNumber.upper(), response)
    return response['team_name']

def setup(opsdroid, other):
    logging.debug("Loaded REvents module")

@match_regex(r'list teams (?P<ID>.*)')
async def list_teams(opsdroid, config, message):
    response = await GetTeam(opsdroid, message.regex.group('ID'))
    await message.respond(str(response))


@match_regex(r'test (?P<redScore>.*)')
async def test(opsdroid, config, message):
    redScore = message.regex.group('redScore')
    # redScore = 'test'
    await opsdroid.memory.put("remember", redScore)
    await message.respond("I'll remember that.")

@match_regex(r'remember')
async def get_data(opsdroid, config, message):
    response = await opsdroid.memory.get("remember")
    await message.respond(response)

@match_regex(r'calc skills (?P<redScore>.*) (?P<blueScore>.*)')
async def last_seen(opsdroid, config, message):
    redScore = message.regex.group('redScore')
    blueScore = message.regex.group('blueScore')
    if (blueScore.isdigit() == True and redScore.isdigit() == True):
        await message.respond("Score:  {}".format(str(int(redScore) - int(blueScore) + 63)))

@match_regex(r'help')
async def help(opsdroid, config, message):
    await message.respond("Robot Events Bot help<br>"
                    "help | shows this help<br>"
                    "calc skills [red's score] [blue's score] | calculates skills score from field score<br>"
                    "list teams [event ID] | get a list of all teams and their info from the given event.<br>"
                    # "get teams | lists all teams at the active event<br>"
                    # "get team [team number] | info about a team<br>"
                    # "get skills [team number] | give teams highest skills rank and score<br>"
                    # "list events [SKU or team number] | lists all events for a city<br>"
                    # "set event [event ID] | sets the event <br>"
                    # "get event | gets the current event<br>"
                    # "list matches [team number] | gets all matches for the given team (NOT IMPLEMENTED YET!)<br>"
                    # "get rankings | gets the current ranking for the active event(NOT IMPLEMENTED YET!)<br>"
                    # "list skills | gets the current skills standings for the active event"
                    )


# @match_always
# async def update_seen(opsdroid, config, message):
#     seen = await opsdroid.memory.get("seen")
#     if seen == None:
#         seen = {}
#     seen[message.user] = datetime.now()
#     await opsdroid.memory.put("seen", seen)