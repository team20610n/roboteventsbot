import json
import requests
import pandas as pd
from time import sleep
from datetime import datetime
import operator

class RobotEvents:
    def __init__(self, APIKey):
        self._APIKey = str(APIKey)
        self._debug = False

    def getRaw(self, query):
        endpoint = "https://www.robotevents.com/api/v2/" + query
        if (self._debug):
            print("requesting: " + endpoint)
        ROBOTEVENTS_HEADER = {"Authorization": "Bearer "+self._APIKey, "accept": "application/json"}
        df_retval = None
        response = requests.get(endpoint,headers=ROBOTEVENTS_HEADER)
        json_results = json.loads(response.content.decode('utf-8'))
        try:
            df_retval = pd.read_json(json.dumps(json_results['data']))
        except KeyError:
            raise Exception("malformed request!")

        while json_results['meta']['next_page_url'] is not None:
            if (self._debug):
                print(json_results['meta']['next_page_url'])
            response = requests.get(json_results['meta']['next_page_url'],headers=ROBOTEVENTS_HEADER)
            json_results = json.loads(response.content.decode('utf-8'))
            df_thispage = pd.read_json(json.dumps(json_results['data']))
            df_retval = df_retval.append(df_thispage)

        new_index = []
        for i in range(0, int(json_results['meta']['total'])):
            if (self._debug):
                print("Grabing aditional pages " + str(i))
            new_index.append(i)
        df_retval.index = new_index
        df_retval = df_retval.to_dict()

        return df_retval

    def seasonID(self, ID=None, program=None, teamNumber=None, active=True):
        """
        """
        quorry = '?'
        joiner = ''
        if (ID != None):
            quorry += 'id%5B%5D='+str(ID)
            joiner = '&'
        if (program != None):
            quorry += joiner+'program%5B%5D='+str(program)
            joiner = '&'
        if (teamNumber != None):
            quorry += joiner+'team%5B%5D='+str(teamNumber)
            joiner = '&'
        if (active == True):
            quorry += joiner+'active=true'
            joiner = '&'
        # if (endTime != None):
        #     quorry += joiner+'end%5B%5D='+str(EventSKU).upper()
        #     joiner = '&'

        if (self._debug):
            print("seasons"+quorry)
        return self.getRaw("seasons"+quorry)
        # seasons?id%5B%5D=139&program%5B%5D=1&team%5B%5D=84549&active=true"

    def currentSeasonID(self, EventType):
        """
        Excepted event types: 
            VRC, IQ, VEXU, VAIC-HS, VAIC-U, RAD, and VEXU.
        Otherwise just put the event id number in.
        """
        self.data = None
        if (isinstance(EventType, int)):
            self.data = self.getRaw("seasons?program%5B%5D=" + str(EventType) + "&active=true")
        elif (EventType.upper() == 'VRC'):
            self.data = self.getRaw("seasons?program%5B%5D=1&active=true")
        elif (EventType.upper() == 'IQ'):
            self.data = self.getRaw("seasons?program%5B%5D=41&active=true")
        elif (EventType.upper() == "VEXU"):
            self.data = self.getRaw("seasons?program%5B%5D=4&active=true")
        elif (EventType.upper() == "VAIC-HS"):
            self.data = self.getRaw("seasons?program%5B%5D=4&active=true")
        elif (EventType.upper() == "VAIC-U"):
            self.data = self.getRaw("seasons?program%5B%5D=4&active=true")
        elif (EventType.upper() == "RAD"):
            self.data = self.getRaw("seasons?program%5B%5D=4&active=true")
        elif (EventType.upper() == "VEXU"):
            self.data = self.getRaw("seasons?program%5B%5D=4&active=true")

        IDs = []
        for i in range(len(self.data['id'])):
            IDs.append(self.data['id'][i])
        return min(IDs)
    
    def teamData(self, number):
        return self.getRaw("teams?number%5B%5D=" + str(number))
    
    def teamID(self, number):
        try:
            return self.teamData(number)['id'][0]
        except KeyError:
            raise Exception("Not a valid team number!")

    def events(self, SeasonID, EventSKU = None, teamNumber = None, level = None, startTime = None, endTime = None):
        """
        startTime and endTime isn't implemented yet.
        
        While SeasonID is the only required filter it's highly recomended to filteras much as possible to reduce quorries.
        
        level possible inputs:
            World
            National (regoinal or normal events)
            State
            Signature
            Other

        Example:
            RobotEvents.events(RobotEvents.currentSeasonID(1), 'RE-VRC-21-3849', '20610N', "states")
        """

        quorry = '?'
        joiner = ''
        if (EventSKU != None):
            quorry += 'sku%5B%5D='+str(EventSKU).upper()
            joiner = '&'
        if (teamNumber != None):
            quorry += joiner+'team%5B%5D='+str(self.teamID(teamNumber))
            joiner = '&'
        if (level != None):
            quorry += joiner+'level%5B%5D='+str(level).capitalize()
            joiner = '&'
        if (startTime != None):
            quorry += joiner+'start%5B%5D='+str(startTime)
            joiner = '&'
        # if (endTime != None):
        #     quorry += joiner+'end%5B%5D='+str(EventSKU).upper()
        #     joiner = '&'

        if (self._debug):
            print("RobotEventsV2Py:Events:OutPut seasons/"+str(SeasonID)+"/events"+quorry)
        return self.getRaw("seasons/"+str(SeasonID)+"/events"+quorry)

if __name__ == "__main__":
    APIKey = None
    with open("RobotEventsKey", 'r') as _APIKey:
        APIKey = _APIKey.read()
    RobotEvents = RobotEvents(APIKey)
    RobotEvents._debug = True
    print(RobotEvents.events(RobotEvents.currentSeasonID(1), teamNumber='20610N', level='state', startTime='a'))
    # print(RobotEvents.teamID('20610N'))
    # https://www.robotevents.com/api/v2/seasons/139/events?team%5B%5D=84549&level%5B%5D=State&start[]=a